extends Spatial

var active = false
onready var snd = $sound

func _process(delta):
	if active:
		active = false
		if !snd.is_playing():
			snd.play()
		
