extends Spatial

var image_1 = preload("res://assest/objects/radio_room_monitor/gnfd_desktop.jpg")
var image_2 = preload("res://assest/objects/radio_room_monitor/gnfd_desktop_2.jpg")

var images = [ image_1, image_2 ]
var img = 0

func change_image():
	img += 1
	if img >= images.size():
		img = 0

	# Get the material in slot 0
	var material_one = $Cube.mesh.surface_get_material(1)
	# Change the texture
	material_one.albedo_texture = images[img]

